module.exports = formatResponse = (data = undefined, errors = undefined) => {
    let response = {};
    if (data !== undefined) {
        response.data = data;
    }
    if (errors !== undefined) {
        response.errors = errors;
    }
    return response;
}