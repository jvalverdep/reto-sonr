const passport = require('passport');
const secret = require('../config/keys').secret;
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const LocalStrategy = require('passport-local');

module.exports = (app) => {
    const db = app.db;
    passport.use(
        new LocalStrategy(function(username, password, done) {
            db.user.findOne({ where: { username }})
                .then(user => {
                    if (!user) { return done(null, false); }
                    user.comparePassword(password, function(err, isMatch) {
                        if (err) { return done(err); }
                        if (!isMatch) { return done(null, false); }

                        return done(null, user);
                    })
                })
                .catch(error => {
                    return done(error);
                })
        }));

    const jwtOptions = {
        jwtFromRequest: ExtractJwt.fromHeader('authorization'),
        secretOrKey: secret
    }

    passport.use(
        new JwtStrategy(jwtOptions, function(payload, done) {
            db.user.findByPk(payload.sub)
                .then(user => {
                    if (!user) { done(null, false); }
                    done(null, user);
                });
        }));
    app.use(passport.initialize());
    app.use(passport.session());
}