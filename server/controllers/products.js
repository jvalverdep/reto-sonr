const express = require('express');
const router = express.Router();
const formatResponse = require('../helpers/util');

// Get One
router.get('/:id', async (req, res, next) => {
    const db = req.app.db;
    const id = req.params.id;
    const product = await db.product.findByPk(id, { include: [{ model: db.category }]});
    if (!product) {
        return res.status(200).json(formatResponse(undefined, [{ message: 'El producto no existe.' }]));
    }
    res.status(200).json(formatResponse(product));
});

// All
router.get('/', async (req, res, next) => {
    const db = req.app.db;
    const products = await db.product.findAll({ include: [{ model: db.category }]});
    res.status(200).json(formatResponse(products));
});

module.exports = router;