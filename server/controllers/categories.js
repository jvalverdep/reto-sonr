const express = require('express');
const router = express.Router();
const formatResponse = require('../helpers/util');

// Get products by category
router.get('/:id/products', async (req, res, next) => {
    const db = req.app.db;
    const id = req.params.id;
    const category = await db.category.findByPk(id, { include: [{ model: db.product }]});
    if (!category) {
        return res.status(200).json(formatResponse(undefined, [{ message: 'No se encontró la categoría.' }]));
    }
    res.status(200).json(formatResponse(category));
});

// All
router.get('/', async (req, res, next) => {
    const db = req.app.db;
    const categories = await db.category.findAll();
    res.status(200).json(formatResponse(categories));
});

module.exports = router;