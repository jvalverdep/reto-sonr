const express = require('express');
const router = express.Router();
const passport = require('passport');
const formatResponse = require('../../helpers/util');

const requireSignin = passport.authenticate('local', { session: false });

const jwt = require('jwt-simple');
const secret = require('../../config/keys').secret;

function tokenForUser(user) {
    const timestamp = new Date().getDate();
    return jwt.encode({ sub: user.id, iat: timestamp }, secret);
}

router.post('/signin', requireSignin, (req, res, next) => {
    res.status(200).json(formatResponse({ token: tokenForUser(req.user) }));
});

router.post('/signup', async (req, res, next) => {
    const db = req.app.db;
    const { username, password } = req.body;

    if (!username || !password) {
        return res.status(200).json(formatResponse(undefined, [{ message: 'Debes ingresar usuario y contraseña.' }]));
    }
    
    const userExists = await db.user.findOne({ where: { username: username }});
    if (userExists) {
        return res.status(200).json(formatResponse(undefined, [{ message: 'Usuario ya existe.' }]));
    }

    db.user.create({ username, password }).then(user => {
        res.status(200).json(formatResponse({ token: tokenForUser(user) }));
    }).catch(err => {
        next(err);
    }) 
});

module.exports = router;