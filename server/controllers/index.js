const express = require('express');
const router = express.Router();
const passport = require('passport');

const requireAuth = passport.authenticate('jwt', { session: false });

// Admin Routes
const adminRouter = require('./admin');

// Auth Routes
const authRouter = require('./auth');

// Public Routes
const categories = require('./categories');
const products = require('./products');
router.use('/categories', categories);
router.use('/products', products);

module.exports = (app) => {
    app.use('/api/admin', requireAuth, adminRouter);
    app.use('/api', router);
    app.use('/api', authRouter);
}