const express = require('express');
const router = express.Router();
const formatResponse = require('../../helpers/util');

// user's list

router.get('/', async (req, res, next) => {
    const db = req.app.db;
    const users = await db.user.findAll();
    res.status(200).json(formatResponse(users));
});

// Get One
router.get('/:id', async (req, res, next) => {
    const db = req.app.db;
    const id = req.params.id;
    const user = await db.user.findByPk(id);
    if (!user) {
        return res.status(200).json(formatResponse(undefined, [{ message: 'No se encontró el producto.' }]));
    }
    res.status(200).json(formatResponse(user));
});

// Create user
router.post('/', async (req, res, next) => {
    const db = req.app.db;
    const { username, password } = req.body;

    if (!username || !password) {
        return res.status(200).json(formatResponse(undefined, [{ message: 'Debes ingresar usuario y contraseña.' }]));
    }
    
    const userExists = await db.user.findOne({ where: { username: username }});
    if (userExists) {
        return res.status(200).json(formatResponse(undefined, [{ message: 'Usuario ya existe.' }]));
    }

    db.user.create({ username, password }).then(user => {
        res.status(200).json(formatResponse(user));
    }).catch(err => {
        next(err);
    }) 
});

// Update
router.post('/:id', async (req, res, next) => {
    const db = req.app.db;
    const id = req.params.id;
    const { username, password } = req.body;
    const user = await db.user.findByPk(id);
    if (!user) {
        return res.status(200).json(formatResponse(undefined, [{ message: 'El usuario no existe.' }]));
    }
    user.comparePassword(password, function(err, isMatch) {
        if (!isMatch) {
            user.update(req.body).then(userUpdated => {
                return res.status(200).json(formatResponse(userUpdated));
            });
        } else {
            return res.status(200).json(formatResponse(undefined, [{ message: 'Por favor, ingresa una contraseña diferente a la ya guardada.' }]));
        }
    })
    
});

router.get('/delete/:id', async (req, res, next) => {
    const db = req.app.db;
    const id = req.params.id;

    const user = await db.user.findByPk(id);
    if (!user) {
        return res.status(200).json(formatResponse(undefined, [{ message: 'No se encontró el usuario.' }]));
    }
    await user.destroy();
    res.status(200).json(formatResponse(user));
});

module.exports = router;