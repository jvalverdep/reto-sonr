const express = require('express');
const router = express.Router();
const formatResponse = require('../../helpers/util');

// Get One with products
router.get('/:id/products', async (req, res, next) => {
    const db = req.app.db;
    const id = req.params.id;
    const category = await db.category.findByPk(id, { include: [{ model: db.product }]});
    if (!category) {
        return res.status(200).json(formatResponse(undefined, [{ message: 'No se encontró la categoría.' }]));
    }
    res.status(200).json(formatResponse(category));
});

// Get One
router.get('/:id', async (req, res, next) => {
    const db = req.app.db;
    const id = req.params.id;
    const category = await db.category.findByPk(id);
    if (!category) {
        return res.status(200).json(formatResponse(undefined, [{ message: 'No se encontró la categoría.' }]));
    }
    res.status(200).json(formatResponse(category));
});

// Update
router.post('/:id', async (req, res, next) => {
    const db = req.app.db;
    const id = req.params.id;
    const toUpdate = req.body;
    const category = await db.category.findByPk(id);
    if (!category) {
        return res.status(200).json(formatResponse(undefined, [{ message: 'No se encontró la categoría.' }]));
    }
    category.update(toUpdate).then(categoryUpdated => {
        res.status(200).json(formatResponse(categoryUpdated));
    });
});

// All
router.get('/', async (req, res, next) => {
    const db = req.app.db;
    const categories = await db.category.findAll();
    res.status(200).json(formatResponse(categories));
});

// Create
router.post('/', async (req, res, next) => {
    const { name, description } = req.body;
    const db = req.app.db;
    if (!name) {
        return res.status(200).json(formatResponse(undefined, [{ message: 'Faltan datos' }]));
    }
    db.category.create(req.body).then(newCategory => {
        res.status(201).json(formatResponse(newCategory));
    }).catch(error => {
        next(error);
    });
});

// Delete by id
router.get('/delete/:id', async (req, res, next) => {
    const db = req.app.db;
    const id = req.params.id;

    const category = await db.category.findByPk(id);
    if (!category) {
        return res.status(200).json(formatResponse(undefined, [{ message: 'No se encontró la categoría.' }]));
    }
    await category.destroy();
    res.status(200).json(formatResponse(category));
});

module.exports = router;