const express = require('express');
const router  = express.Router();

const categoriesAdmin = require('./categories');
const productsAdmin = require('./products');
const usersAdmin = require('./users');

router.use("/categories", categoriesAdmin);
router.use("/products", productsAdmin);
router.use("/users", usersAdmin);

module.exports = router;