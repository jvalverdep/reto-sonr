const express = require('express');
const router = express.Router();
const formatResponse = require('../../helpers/util');

// Get One
router.get('/:id', async (req, res, next) => {
    const db = req.app.db;
    const id = req.params.id;
    const product = await db.product.findByPk(id, { include: [{ model: db.category }]});
    if (!product) {
        return res.status(200).json(formatResponse(undefined, [{ message: 'No se encontró el producto.' }]));
    }
    res.status(200).json(formatResponse(product));
});

// Update
router.post('/:id', async (req, res, next) => {
    const db = req.app.db;
    const id = req.params.id;
    const toUpdate = req.body;
    const product = await db.product.findByPk(id);
    if (!product) {
        return res.status(200).json(formatResponse(undefined, [{ message: 'No se encontró el producto.' }]));
    }
    product.update(toUpdate).then(productUpdated => {
        db.product.findByPk(productUpdated.id, { include: [{ model: db.category }]})
            .then(product => {
                res.status(201).json(formatResponse(product));
            });
    });
});

// All
router.get('/', async (req, res, next) => {
    const db = req.app.db;
    const products = await db.product.findAll({ include: [{ model: db.category }]});
    res.status(200).json(formatResponse(products));
});

// Create
router.post('/', async (req, res, next) => {
    const { name, description, price, category_id } = req.body;
    const db = req.app.db;
    if (!name || !price || !category_id) {
        return res.status(200).json(formatResponse(undefined, [{ message: 'Faltan datos' }]));
    }
    const categoryExists = await db.category.findByPk(category_id);
    if (!categoryExists) {
        return res.status(200).json(formatResponse(undefined, [{ message: 'No existe categoría.' }]));
    }
    db.product.create(req.body).then(newProduct => {
        db.product.findByPk(newProduct.id, { include: [{ model: db.category }]})
            .then(product => {
                res.status(201).json(formatResponse(product));
            })
    }).catch(error => {
        next(error);
    });
});

// Delete by Id
router.get('/delete/:id', async (req, res, next) => {
    const db = req.app.db;
    const id = req.params.id;

    const product = await db.product.findByPk(id, { include: [{ model: db.category }]});
    if (!product) {
        return res.status(200).json(formatResponse(undefined, [{ message: 'No se encontró el producto.' }]));
    }
    await product.destroy();
    res.status(200).json(formatResponse(product));
});

module.exports = router;