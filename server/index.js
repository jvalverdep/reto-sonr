const express = require('express');
const path = require('path');
const cookieSession = require('cookie-session');
const passport = require('passport');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const cors = require('cors');

const keys = require("./config/keys");

const app = express();

// Database
app.db = require('./models');

app.use(express.static(path.join(__dirname, "public"), { maxAge: 31557600000 }));

app.use(morgan("common"));
app.use(bodyParser.json());
app.use(cors());
app.use(cookieSession({
    maxAge: 30 * 24 * 60 * 60 * 1000,
    keys: [keys.cookieSessionKey]
}));

// Passport configuration
require('./services/passport')(app);

// Router (handles all routes)
const router = require('./controllers');
router(app);

const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
    console.log(`Listening on port`, PORT);
});