const bcrypt = require('bcrypt');

module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define('user', {
        username: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
        password: {
            type: DataTypes.STRING,
            allowNull: false
        }
    });

    User.beforeCreate(async (user, options) => bcrypt.hash(user.password, 12)
        .then(hash => {
            user.password = hash;
        })
        .catch(err => {
            throw err;
        }));

    User.beforeUpdate(async (user, options) => bcrypt.hash(user.password, 12)
        .then(hash => {
            user.password = hash;
        })
        .catch(err => {
            throw err;
        }));

    User.prototype.comparePassword = function (candidatePassword, callback) {
        bcrypt.compare(candidatePassword, this.password, function (err, isMatch) {
            if (err) {
                return callback(err);
            }

            callback(null, isMatch);
        });
    }

    return User;
}