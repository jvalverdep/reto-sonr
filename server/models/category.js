module.exports = (sequelize, DataTypes) => {
    const Category = sequelize.define('category', {
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        description: {
            type: DataTypes.STRING,
            allowNull: true
        }
    }, {
        timestamps: false
    });

    Category.associate = function(models){
        models.category.hasMany(models.product, { onDelete: 'cascade' });
    }
    return Category;
}