const path = require('path');
const fs = require('fs');
const Sequelize = require('sequelize');

const dbConfig = require('../config/keys').database;

const sequelize = new Sequelize(dbConfig.database, dbConfig.username, dbConfig.password, {
    host: dbConfig.host,
    port: dbConfig.port,
    dialect: dbConfig.dialect,
    logging: false,
    define: {
        underscored: true
    },
    operatorsAliases: false,
    timezone: '-05:00'
});

let db = {};

// Models
const basename = path.basename(__filename);

fs.readdirSync(__dirname)
    .filter(file => {
        return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
    })
    .forEach(file => {
        const model = sequelize.import(path.join(__dirname, file));
        db[model.name] = model;
        console.log(`Model <${model.name}> registered`);
    });

// Relationships
Object.keys(db).forEach(modelName => {
    if (db[modelName].associate) {
        db[modelName].associate(db);
    }
});

sequelize.sync({force: false}).then(() => {
    // Colors
    db.category.count().then( n => {
        if (n == 0) {
            db.category.create({ name: 'Línea Blanca' });
            db.category.create({ name: 'Ropa' });
            db.category.create({ name: 'Juguetes' });
            db.category.create({ name: 'Cómputo' });
        }
    });
    db.user.count().then(n => {
        if (n == 0) {
            db.user.create({ username: 'admin', password: 'admin' });
        }
    });
});



db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;