Reto-blazar
Aplicación de Ecommerce con administrador
Front:
    React, Redux, thunks
Back:
    Node.js, Express, MySQL, Passport, Jwt

### Instalación
Antes de comenzar, es neceario crear una base de datos en Mysql.
Los datos de la base de datos deberán ser actualizados en el archivo
/server/config/dev.js

```
> git clone 
https://gitlab.com/jvalverdep/reto-sonr.git
> cd server
> npm install
> npm start
> open -a Terminal "`pwd`"
> cd ../client
> npm install
> npm start
```