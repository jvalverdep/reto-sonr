const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const webpack = require('webpack');

module.exports = {
    mode: "development",
    watch: true,
    watchOptions: {
        ignored: /node_modules/
    },
    entry: {
        main: './src/index.js'
    },
    devtool: 'inline-source-map',
    devServer: {
        historyApiFallback: true,
        publicPath: 'http://localhost:8080/assets/',
        hot: true
    },
    plugins: [
        new CleanWebpackPlugin(['dist']),
        new HtmlWebpackPlugin({
            title: 'Hot Module Replacement',
            template: 'index.html'
        }),
        new webpack.HotModuleReplacementPlugin()
    ],
    output: {
        path: path.resolve(__dirname, 'dist/assets'),
        filename: '[name].bundle.js',
        chunkFilename: '[id].chunk.js',
        publicPath: 'http://localhost:8080/assets/',
    },
    resolve: {
        extensions: [".js", ".jsx", ".json", ".scss"]
    },
    module: {
        rules: [
            {
                test: /\.js$/, 
                loader: "source-map-loader",
                enforce: "pre"
            },
            { 
                test: /\.js$/, 
                loader: "babel-loader",
                exclude: /node_modules/ 
            },
            {
                test: /\.css$/,
                use: [{
                    loader: "style-loader"
                }, 
                {
                    loader: "css-loader", 
                    options: {
                        sourceMap: true,
                        importLoaders: 1
                    }
                }, 
                {
                    loader: "postcss-loader"
                }]
            },
            {
                test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                use: [{
                    loader: "file-loader"
                }]
            }
        ]
    }
}