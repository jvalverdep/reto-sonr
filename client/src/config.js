const ROOT_URL = 'http://localhost:5000/api';

const ADMIN_URL = `${ROOT_URL}/admin`;

export const ADMIN_CATEGORY_URL = `${ADMIN_URL}/categories`;
export const ADMIN_PRODUCT_URL = `${ADMIN_URL}/products`;
export const ADMIN_USER_URL = `${ADMIN_URL}/users`;

export const CATEGORY_URL = `${ROOT_URL}/categories`;
export const PRODUCT_URL = `${ROOT_URL}/products`;

export const AUTH_URL = `${ROOT_URL}/auth`;