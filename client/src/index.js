import React from 'react'
import { render } from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import reduxThunk from 'redux-thunk';
import rootReducer from './reducers/index';
import Root from './components/Root';

import './index.css';

export const store = createStore(rootReducer, applyMiddleware(reduxThunk));

if (localStorage.getItem('token')) {
    store.dispatch({ type: 'AUTH_USER' });
}

render(<Root store={store} />, document.getElementById('root'));