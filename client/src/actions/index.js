import axios from 'axios';
import { FETCH_CATEGORIES, FETCH_PRODUCTS, FILTER_PRODUCTS, FETCH_PRODUCT} from './types';
import { CATEGORY_URL, PRODUCT_URL } from '../config';

const fetchCategories = (categories) => {
    return {
        type: FETCH_CATEGORIES,
        categories
    }
}

const fetchProducts = (products) => {
    return {
        type: FETCH_PRODUCTS,
        products
    }
}

export const fetchAllCategories = () => async dispatch => {
    const response = await axios.get(CATEGORY_URL);
    dispatch(fetchCategories(response.data.data));
}

export const fetchAllProducts = () => async dispatch => {
    const response = await axios.get(PRODUCT_URL);
    dispatch(fetchProducts(response.data.data));
}

export const fetchProduct = (id) => async dispatch => {
    const response = await axios.get(`${PRODUCT_URL}/${id}`);
    dispatch({ type: FETCH_PRODUCT, product: response.data.data });
}

export const filterProducts = (id) => {
    return {
        type: FILTER_PRODUCTS,
        category_id: id
    }
}

export const filterProductsByCategoryId = (id) => async dispatch => {
    const response = await axios.get(PRODUCT_URL);
    dispatch(fetchProducts(response.data.data));
    dispatch(filterProducts(id));
}