import axios from 'axios';
import { browserHistory } from '../components/Root';
import { AUTH_USER, UNAUTH_USER, AUTH_ERROR } from './types';
import { AUTH_URL } from '../config';
import { toastr } from 'react-redux-toastr';

export function authError(error) {
    return {
        type: AUTH_ERROR,
        payload: error
    };
}

export function authSuccess() {
    return {
        type: AUTH_USER
    }
}

export function signInUser({ username, password }) {
    return function(dispatch) { 
        axios.post(`${AUTH_URL}/signin`, { username, password })
            .then(response => {
                dispatch(authSuccess());
                dispatch(() => toastr.info("Bienvenido!"));
                localStorage.setItem('token', response.data.data.token);
                browserHistory.push('/admin');
            })
            .catch(() => {
                dispatch(authError('Bad Login Info'));
            });
    }
}

export function signOutUser() {
    localStorage.removeItem('token');

    return { type: UNAUTH_USER };
}