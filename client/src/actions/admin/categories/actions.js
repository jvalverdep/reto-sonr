import axios from 'axios';
import { toastr } from 'react-redux-toastr';

import { ADD_CATEGORY_SUCCESS, UPDATE_CATEGORY_SUCCESS, DELETE_CATEGORY_SUCCESS, CATEGORY_ERROR } from './types';
import { FETCH_CATEGORIES, FETCH_CATEGORY } from '../../types';
import { ADMIN_CATEGORY_URL } from '../../../config';

const fetchCategories = (categories) => {
    return {
        type: FETCH_CATEGORIES,
        categories
    }
}

export const fetchAllCategories = () => async dispatch => {
    const response = await axios.get(ADMIN_CATEGORY_URL, { headers: localStorage.getItem('token') !== null ? {'authorization': localStorage.getItem('token')}: {}});
    dispatch(fetchCategories(response.data.data));
}

export const fetchCategory = (id) => async dispatch => {
    const response = await axios.get(`${ADMIN_CATEGORY_URL}/${id}`, { headers: localStorage.getItem('token') !== null ? {'authorization': localStorage.getItem('token')}: {}});
    dispatch({ type: FETCH_CATEGORY, category: response.data.data });
}

export const createCategory = ({ name, description }) => async dispatch => {
    const response = await axios.post(ADMIN_CATEGORY_URL, { name, description }, { headers: localStorage.getItem('token') !== null ? {'authorization': localStorage.getItem('token')}: {}});
    if (response.data.errors) {
        const errors = response.data.errors;
        return Promise.all([
            dispatch(categoryError(errors)),
            dispatch(() => toastr.error("Error!", errors[0].message))
        ]);
    }
    return Promise.all([
        dispatch(createCategorySuccess(response.data.data)),
        dispatch(() => toastr.success("Éxito!", "La categoría fue creada exitosamente!"))
    ]);
}

export const updateCategory = ({ id, name, description }) => async dispatch => {
    const response = await axios.post(`${ADMIN_CATEGORY_URL}/${id}`, { name, description }, { headers: localStorage.getItem('token') !== null ? {'authorization': localStorage.getItem('token')}: {}});
    if (response.data.errors) {
        const errors = response.data.errors;
        return Promise.all([
            dispatch(categoryError(errors)),
            dispatch(() => toastr.error("Error!", errors[0].message))
        ]);
    }
    return Promise.all([
        dispatch(updateCategorySuccess(response.data.data)),
        dispatch(() => toastr.success("Éxito!", "La categoría fue actualizada exitosamente!"))
    ]);
}

export const deleteCategory = id => async dispatch => {
    const response = await axios.get(`${ADMIN_CATEGORY_URL}/delete/${id}`, { headers: localStorage.getItem('token') !== null ? {'authorization': localStorage.getItem('token')}: {}});
    if (response.data.errors) {
        const errors = response.data.errors;
        return Promise.all([
            dispatch(categoryError(errors)),
            dispatch(() => toastr.error("Error!", errors[0].message))
        ]);
    }
    
    return Promise.all([
        dispatch(deleteCategorySuccess(response.data.data)),
        dispatch(() => toastr.success("Éxito!", "La categoría fue eliminada exitosamente."))
    ]);
}

const createCategorySuccess = ({ id, name, description }) => {
    return {
        type: ADD_CATEGORY_SUCCESS,
        payload: { id, name, description }
    };
};

const updateCategorySuccess = ({ id, name, description }) => {
    return {
        type: UPDATE_CATEGORY_SUCCESS,
        payload: { id, name, description }
    };
};

const deleteCategorySuccess = ({ id }) => {
    return {
        type: DELETE_CATEGORY_SUCCESS,
        payload: { id }
    };
};

const categoryError = () => {
    return {
        type: CATEGORY_ERROR
    };
};