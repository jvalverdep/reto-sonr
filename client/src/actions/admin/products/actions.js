import axios from 'axios';
import { toastr } from 'react-redux-toastr';

import { ADD_PRODUCT_SUCCESS, UPDATE_PRODUCT_SUCCESS, DELETE_PRODUCT_SUCCESS, PRODUCT_ERROR } from './types';
import { FETCH_PRODUCTS, FETCH_PRODUCT } from '../../types';
import { ADMIN_PRODUCT_URL } from '../../../config';

const fetchProducts = (products) => {
    return {
        type: FETCH_PRODUCTS,
        products
    }
}

export const fetchAllProducts = () => async dispatch => {
    const response = await axios.get(ADMIN_PRODUCT_URL, { headers: localStorage.getItem('token') !== null ? {'authorization': localStorage.getItem('token')}: {}});
    dispatch(fetchProducts(response.data.data));
}

export const fetchProduct = (id) => async dispatch => {
    const response = await axios.get(`${ADMIN_PRODUCT_URL}/${id}`, { headers: localStorage.getItem('token') !== null ? {'authorization': localStorage.getItem('token')}: {}});
    dispatch({ type: FETCH_PRODUCT, product: response.data.data });
}

export const createProduct = ({ name, description, price, category }) => async dispatch => {
    const response = await axios.post(ADMIN_PRODUCT_URL, { name, description, price, category_id: category }, { headers: localStorage.getItem('token') !== null ? {'authorization': localStorage.getItem('token')}: {}});
    if (response.data.errors) {
        const errors = response.data.errors;
        return Promise.all([
            dispatch(productError(errors)),
            dispatch(() => toastr.error("Error!", errors[0].message))
        ]);
    }
    return Promise.all([
        dispatch(createProductSuccess(response.data.data)),
        dispatch(() => toastr.success("Éxito!", "El producto fue creado exitosamente!"))
    ]);
}

export const updateProduct = ({ id, name, description, price, category_id }) => async dispatch => {
    const response = await axios.post(`${ADMIN_PRODUCT_URL}/${id}`, { name, description, price, category_id }, { headers: localStorage.getItem('token') !== null ? {'authorization': localStorage.getItem('token')}: {}});
    if (response.data.errors) {
        const errors = response.data.errors;
        return Promise.all([
            dispatch(productError(errors)),
            dispatch(() => toastr.error("Error!", errors[0].message))
        ]);
    }
    return Promise.all([
        dispatch(updateProductSuccess(response.data.data)),
        dispatch(() => toastr.success("Éxito!", "El producto fue actualizado exitosamente!"))
    ]);
}

export const deleteProduct = id => async dispatch => {
    const response = await axios.get(`${ADMIN_PRODUCT_URL}/delete/${id}`, { headers: localStorage.getItem('token') !== null ? {'authorization': localStorage.getItem('token')}: {}});
    if (response.data.errors) {
        const errors = response.data.errors;
        return Promise.all([
            dispatch(productError(errors)),
            dispatch(() => toastr.error("Error!", errors[0].message))
        ]);
    }
    
    return Promise.all([
        dispatch(deleteProductSuccess(response.data.data)),
        dispatch(() => toastr.success("Éxito!", "El producto fue eliminado exitosamente."))
    ]);
}

const createProductSuccess = ({ id, name, description, price, category }) => {
    return {
        type: ADD_PRODUCT_SUCCESS,
        payload: { id, name, description, price, category }
    };
};

const updateProductSuccess = ({ id, name, description, price, category }) => {
    return {
        type: UPDATE_PRODUCT_SUCCESS,
        payload: { id, name, description, price, category }
    };
};

const deleteProductSuccess = ({ id }) => {
    return {
        type: DELETE_PRODUCT_SUCCESS,
        payload: { id }
    };
};

const productError = () => {
    return {
        type: PRODUCT_ERROR
    };
};