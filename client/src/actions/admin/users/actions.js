import axios from 'axios';
import { toastr } from 'react-redux-toastr';

import { ADD_USER_SUCCESS, EDIT_USER_SUCCESS, DELETE_USER_SUCCESS, USER_ERROR, FETCH_USERS } from './types';
import { ADMIN_USER_URL } from '../../../config';

const fetchUsers = (users) => {
    return {
        type: FETCH_USERS,
        users
    }
}

export const fetchAllUsers = () => async dispatch => {
    const response = await axios.get(ADMIN_USER_URL, { headers: localStorage.getItem('token') !== null ? {'authorization': localStorage.getItem('token')}: {}});
    dispatch(fetchUsers(response.data.data));
}

export const createUser = ({ username, password }) => async dispatch => {
    const response = await axios.post(ADMIN_USER_URL, { username, password }, { headers: localStorage.getItem('token') !== null ? {'authorization': localStorage.getItem('token')}: {}});
    if (response.data.errors) {
        const errors = response.data.errors;
        return Promise.all([
            dispatch(userError(errors)),
            dispatch(() => toastr.error("Error!", errors[0].message))
        ]);
    }
    return Promise.all([
        dispatch(createUserSuccess(response.data.data)),
        dispatch(() => toastr.success("Éxito!", "El usuario fue creado exitosamente!"))
    ]);
}

export const deleteUser = id => async dispatch => {
    const response = await axios.get(`${ADMIN_USER_URL}/delete/${id}`, { headers: localStorage.getItem('token') !== null ? {'authorization': localStorage.getItem('token')}: {}});
    if (response.data.errors) {
        const errors = response.data.errors;
        return Promise.all([
            dispatch(userError(errors)),
            dispatch(() => toastr.error("Error!", errors[0].message))
        ]);
    }
    
    return Promise.all([
        dispatch(deleteUserSuccess(response.data.data)),
        dispatch(() => toastr.success("Éxito!", "El usuario fue eliminado exitosamente."))
    ]);
}

const createUserSuccess = ({ id, username, password }) => {
    return {
        type: ADD_USER_SUCCESS,
        payload: { id, username, password }
    };
};

const deleteUserSuccess = ({ id }) => {
    return {
        type: DELETE_USER_SUCCESS,
        payload: { id }
    };
};

const userError = () => {
    return {
        type: USER_ERROR
    };
};