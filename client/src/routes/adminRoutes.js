import Categories from '../containers/admin/categories/Categories';
import Products from '../containers/admin/products/Products';
import Users from '../containers/admin/users/Users';

export default [
    {
        path: '/products',
        name: 'Productos',
        component: Products,
        icon: 'icon-calendar',
        layout: '/admin'
    },
    {
        path: '/categories',
        name: 'Categorías',
        component: Categories,
        icon: 'icon-calendar',
        layout: '/admin'
    },
    {
        path: '/users',
        name: 'Usuarios',
        component: Users,
        icon: 'icon-calendar',
        layout: '/admin'
    }
]