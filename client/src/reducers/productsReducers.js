import { DELETE_PRODUCT_SUCCESS, ADD_PRODUCT_SUCCESS, UPDATE_PRODUCT_SUCCESS, PRODUCT_ERROR } from "../actions/admin/products/types";
import { FETCH_PRODUCTS, FILTER_PRODUCTS, FETCH_PRODUCT } from "../actions/types";

const initialState = {
    products: [],
    product: {}
}

export default function(state = initialState, action) {
    switch (action.type) {
        case FETCH_PRODUCTS:
            return { products: action.products };
        case FILTER_PRODUCTS:
            return { products: state.products.filter(product => product.category_id === action.category_id )};
        case FETCH_PRODUCT:
            return { ...state, product: action.product };
        case ADD_PRODUCT_SUCCESS:
            return { ...state, products: [...state.products, action.payload]}
        case UPDATE_PRODUCT_SUCCESS:
            return { ...state, products: [ ...state.products.filter(product => product.id !== action.payload.id), action.payload ] }
        case DELETE_PRODUCT_SUCCESS:
            return { products: state.products.filter(product => product.id !== action.payload.id) };
        case PRODUCT_ERROR:
            return {...state, products: [...state.products]};
        default:
            return state;
    }
}