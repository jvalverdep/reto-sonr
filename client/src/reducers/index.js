import { combineReducers } from 'redux';
import { reducer as form } from 'redux-form';
import { reducer as toastrReducer } from 'react-redux-toastr';
import categories from './categoriesReducers';
import products from './productsReducers';
import users from './usersReducers';
import auth from './authReducer';

const rootReducer = combineReducers({
    form,
    auth,
    categories,
    products,
    users,
    toastr: toastrReducer
});

export default rootReducer;