import { DELETE_CATEGORY_SUCCESS, ADD_CATEGORY_SUCCESS, UPDATE_CATEGORY_SUCCESS, CATEGORY_ERROR } from "../actions/admin/categories/types";
import { FETCH_CATEGORIES, FETCH_CATEGORY } from "../actions/types";

const initialState = {
    categories: [],
    category: {}
}

export default function(state = initialState, action) {
    switch (action.type) {
        case FETCH_CATEGORIES:
            return { categories: action.categories };
        case FETCH_CATEGORY:
            return { ...state, category: action.category };
        case ADD_CATEGORY_SUCCESS:
            return { ...state, categories: [...state.categories, action.payload]}
        case UPDATE_CATEGORY_SUCCESS:
            return { ...state, categories: [ ...state.categories.filter(category => category.id !== action.payload.id), action.payload ] }
        case DELETE_CATEGORY_SUCCESS:
            return { categories: state.categories.filter(category => category.id !== action.payload.id) };
        case CATEGORY_ERROR:
            return {...state, categories: [...state.categories]};
        default:
            return state;
    }
}