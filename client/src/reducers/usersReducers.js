import { DELETE_USER_SUCCESS, ADD_USER_SUCCESS, USER_ERROR, FETCH_USERS } from "../actions/admin/users/types";

const initialState = {
    users: []
}

export default function(state = initialState, action) {
    switch (action.type) {
        case FETCH_USERS:
            return { users: action.users };
        case ADD_USER_SUCCESS:
            return { ...state, users: [...state.users, action.payload]}
        case DELETE_USER_SUCCESS:
            return { users: state.users.filter(user => user.id !== action.payload.id) };
        case USER_ERROR:
            return {...state, users: [...state.users]};
        default:
            return state;
    }
}