import React, { Component } from 'react'
import { Nav, Collapse, NavItem } from 'reactstrap';
import { Link, NavLink } from 'react-router-dom';

import ReactPerfectScrollbar from 'react-perfect-scrollbar';

import './AdminSidebar.css';

export default class DefaultSidebar extends Component {
    constructor(props) {
        super(props);
        this.state = this.getCollapseStates(props.routes);
        this.getCollapseStates = this.getCollapseStates.bind(this);
    }
    
    collapseItemClickHandler(e, collapseState) {
        e.preventDefault();
        this.setState(prevState => ({
            [collapseState]: !prevState[collapseState]
        }));
    };

    getCollapseStates(routes) {
        let initialState = {};
        return routes.map((route, key) => {
            return route.collapse && (
                initialState = {
                    [route.state]: this.getCollapseInitialState(route.children),
                    ...this.getCollapseStates(route.children),
                    ...initialState
                }
            ), null
        }), initialState;
    }

    navList(routes) {
        return routes.map((item, index) => {
            return this.navType(item, index);
        })
    }

    navType(item, index) {
        if (item.title) {
            return this.navTitle(item, index);
        }
        if (item.divider) {
            return this.navDivider(item, index);
        }
        // if (item.label) {
        //     return this.navLabel(item, index);
        // }
        if (item.collapse) {
            return this.navCollapse(item, index);
        }
        return this.navItem(item, index);
    }

    navTitle(title, key) {
        let classes = ["nav-title"];
        if (Array.isArray(title.classes)) {
            classes = [...classes, ...title.classes];
        }
        return (
            <li key={key} className={classes.join(" ")}>
                {this.navWrapper(title)}
            </li>
        )
    }

    navWrapper(item) {
        if (item.wrapper && item.wrapper.element) {
            return React.createElement(item.wrapper.element, item.wrapper.attributes, item.name);
        }
        return item.name;
    }
    
    navDivider(item, key) {
        let classes = ["divider"];
        if (Array.isArray(item.classes)) {
            classes = [...classes, ...item.classes];
        }
        return <li key={key} className={classes.join(" ")}></li>;
    }

    navLabel(item, key) {

    }
    
    navCollapse(item, key) {
        const iconClass = ["nav-icon", item.icon];
        return (
            <NavItem className={this.state[item.state]? "open": ""} key={key}>
                <NavLink className="nav-link"
                data-toggle="collapse" 
                aria-expanded={this.state[item.state]} 
                to="#" 
                onClick={ e => this.collapseItemClickHandler(e, item.state)}>
                    {/* <i className={iconClass.join(" ")}></i> */}
                    {item.name}
                </NavLink>
                <Collapse isOpen={this.state[item.state]}>
                    <ul className="nav-collapse-items">
                        {this.navList(item.children)}
                    </ul>
                </Collapse>
            </NavItem>
        )
    }

    navItem(item, key) {
        return this.navLink(item, key);
    }

    navLink(item, key) {
        const iconClass = ["nav-icon", item.icon];
        return (
            <NavItem key={key}>
                <NavLink to={item.layout+item.path} className="nav-link">
                    {/* <i className={iconClass.join(" ")}></i> */}
                    {item.name}
                </NavLink>
            </NavItem>
        )
    }

    navBadge(badge) {

    }

    getCollapseInitialState(children) {
        for (let a = 0; a < children.length; a++) {
            if(children[a].collapse&&this.getCollapseInitialState(children[a].children))return true;
            if(-1!==window.location.href.indexOf(children[a].path))return true;
        }
        return false;
    }

    render() {
        return (
            <div className="sidebar">
                <ReactPerfectScrollbar className="sidebar-nav" style={{width: "215px"}}>
                    <Nav style={{width: "215px"}}>
                        {this.navList(this.props.routes)}
                        <NavItem>
                            <NavLink to="/auth/signout" className="nav-link">
                                Cerrar Sesión
                            </NavLink>
                        </NavItem>
                    </Nav>
                </ReactPerfectScrollbar>
            </div>
        )
    }
}