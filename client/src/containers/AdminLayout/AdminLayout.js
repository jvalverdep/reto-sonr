import React, { Component, lazy, Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { Container } from 'reactstrap';
import { AppHeader } from '@coreui/react';

import routes from '../../routes/adminRoutes';
import AdminSidebar from './AdminSidebar';

import Product from '../admin/products/Product';
import Category from '../admin/categories/Category';

const AdminHeader = lazy(() => import('./AdminHeader'));
// const Product = lazy(() => import('../admin/products/Product'));
// const Category = lazy(() => import('../admin/categories/Category'));

export default class AdminLayout extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sidebarOpen: true
        }

        this.sidebarToggleClickHandler = this.sidebarToggleClickHandler.bind(this);
    }
    loading() { 
        return <div className="animated fadeIn pt-1 text-center">Loading...</div>
    };

    sidebarToggleClickHandler() {
        this.setState(prevState => {
            return { sidebarOpen: !prevState.sidebarOpen };
        })
    }

    getRoutes(routes) {
        return routes.map((route, key) => {
            if (route.collapse) {
                return this.getRoutes(route.children);
            }
            if (route.component) {
                return (
                    <Route path={route.layout+route.path}
                    component={route.component}
                    key={key} />
                )
            }
            return null;
        });
    }

    render() {
        let appClasses = ["app"];
        if (this.state.sidebarOpen) {
            appClasses.push("sidebar-show");
        }
        return (
            <div className={appClasses.join(" ")}>
                <AppHeader fixed>
                    <Suspense fallback={this.loading()}>
                        <AdminHeader {...this.props}
                            sidebarToggleClick = {this.sidebarToggleClickHandler}
                        />
                    </Suspense>
                </AppHeader>
                <div className="app-body">
                    <Suspense fallback={this.loading()}>
                        <AdminSidebar 
                            {...this.props}
                            routes={routes}
                            show={this.state.sidebarOpen}
                        />
                    </Suspense>
                    <main className="main">
                        <Container fluid>
                            <Suspense fallback={this.loading()}>
                                <Switch>
                                    <Route exact path="/admin/products/:id" name="Product" component={Product} />
                                    <Route exact path="/admin/categories/:id" name="Category" component={Category} />
                                    {this.getRoutes(routes)}
                                </Switch>
                            </Suspense>
                        </Container>
                    </main>
                </div>
            </div>
        )
    }
}