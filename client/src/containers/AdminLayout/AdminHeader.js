import React, { Component, Fragment } from 'react';
import { Container, Navbar, NavbarBrand } from 'reactstrap';
import { NavLink } from 'react-router-dom';

import './AdminHeader.css';

export default class DefaultHeader extends Component {
    render() {
        return (
            <Navbar light expand="md">
                <NavLink className="navbar-brand" to="/admin">
                    Administrador Ecommerce
                </NavLink>
            </Navbar>
        )
    }
}