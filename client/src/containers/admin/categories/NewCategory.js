import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm, Form } from 'redux-form';
import { Modal, ModalHeader, ModalBody, FormGroup } from 'reactstrap';

import { createCategory } from '../../../actions/admin/categories/actions';

class NewCategory extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            description: ''
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleReset = this.handleReset.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState(prevState => ({
            ...prevState,
            [name]: value
        }));
    }

    handleSubmit(e) {
        e.preventDefault();
        if (this.state.name.trim()) {
            this.props.createCategory(this.state);
            this.handleReset();
            this.props.handleToggle();
        }
    }

    handleReset() {
        this.setState({
            name: '',
            description: ''
        });
    }
    
    render() {
        return (
            <div>
                <Modal isOpen={this.props.show} toggle={this.props.handleToggle} className={this.props.className}>
                    <ModalHeader toggle={this.props.handleToggle}>Nueva Categoría</ModalHeader>
                    <ModalBody>
                        <Form onSubmit={this.handleSubmit}>
                            <FormGroup>
                                <label htmlFor="name">Nombre</label>
                                <input
                                id="name"
                                name="name"
                                type="text"
                                onChange={this.handleInputChange}
                                value={this.state.name}
                                className="form-control"
                                />
                            </FormGroup>
                            <FormGroup>
                                <label htmlFor="description">Descripción</label>
                                <input
                                id="description"
                                name="description"
                                type="text"
                                onChange={this.handleInputChange}
                                value={this.state.description}
                                className="form-control"
                                />
                            </FormGroup>
                            <button type="submit" className="btn btn-primary">Crear</button>{' '}
                            <button type="button" onClick={this.props.handleToggle} className="btn btn-secondary">Cancelar</button>
                        </Form>
                    </ModalBody>
            </Modal>
          </div>
        )
    }
}

export default reduxForm({ 
    form: 'categoryForm' 
})(
    connect(null, { createCategory })(NewCategory)
);