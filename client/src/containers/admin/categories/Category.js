import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm, Form } from 'redux-form';
import { NavLink } from 'react-router-dom';
import { Modal, ModalHeader, ModalBody, FormGroup, Container } from 'reactstrap';
import { fetchCategory, updateCategory } from '../../../actions/admin/categories/actions';

class Category extends Component {
    constructor(props) {
        super(props);
        this.state = {
            category: {
                id: '',
                name: '',
                description: ''
            }
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleReset = this.handleReset.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState(prevState => ({
            category: {
                ...prevState.category,
                [name]: value
            }
        }));
        // this.setState(prevState => ({
        //     company: {
        //         ...prevState.company,
        //         [name]: value
        //     }
        // }));
    }

    handleSubmit(e) {
        e.preventDefault();
        const { name, description } = this.state.category;
        if (name.trim()) {
            this.props.updateCategory(this.state.category);
            this.handleReset();
            this.props.history.push('/admin/categories');
        }
    }

    handleReset() {
        this.setState({
            category: {
                id: '',
                name: '',
                description: ''
            }
        });
    }

    componentDidMount() {
        this.props.fetchCategory(this.props.match.params.id).then(() => 
            this.setState({
                category: this.props.category
            })
        );
    }

    render() {
        const { category } = this.props;
        if (!category) {
            return <div>Esta categoría no existe</div>
        } else {
            return (
                <Container>
                    <Form onSubmit={this.handleSubmit}>
                        <FormGroup>
                            <label htmlFor="name">Nombre</label>
                            <input
                            id="name"
                            name="name"
                            type="text"
                            onChange={this.handleInputChange}
                            value={this.state.category.name}
                            className="form-control"
                            />
                        </FormGroup>
                        <FormGroup>
                            <label htmlFor="description">Descripción</label>
                            <input
                            id="description"
                            name="description"
                            type="text"
                            onChange={this.handleInputChange}
                            value={this.state.category.description}
                            className="form-control"
                            />
                        </FormGroup>
                        <button type="submit" className="btn btn-primary">Actualizar</button>{' '}
                        {/* <button type="button" className="btn btn-secondary">Cancelar</button> */}
                        <NavLink style={{color: "white"}} to="/admin/categories" className="button btn btn-secondary">
                        Cancelar
                        </NavLink>
                    </Form>
                </Container>
            )
        }
    }
}

const mapStateToProps = ({ categories }) => {
    return { 
        category: categories.category
    };
};

export default reduxForm({ 
    form: 'editCategoryForm' 
})(
    connect(mapStateToProps, { fetchCategory, updateCategory })(Category)
);