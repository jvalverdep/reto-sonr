import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Card, CardBody, Table } from 'reactstrap';
import { fetchAllCategories, deleteCategory } from '../../../actions/admin/categories/actions';

import Category from '../../../components/admin/categories/Category';

class CategoriesList extends Component {
    constructor(props) {
        super(props);

        this.deleteCategoryHandler = this.deleteCategoryHandler.bind(this);
        this.renderCategories = this.renderCategories.bind(this);
    }
    componentDidMount() {
        this.props.fetchAllCategories();
    }
    deleteCategoryHandler(id) {
        this.props.deleteCategory(id);
    }
    renderCategories(categories) {
        if (categories.length > 0) {
            return (
                <Table responsive>
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Descripción</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        {categories.map((category, key) => {
                            return (
                                <Category category={category} key={key} onDelete={this.deleteCategoryHandler}/>
                            );
                        })}
                    </tbody>
                </Table>
            );
        } else {
            return <p>No existen categorías.</p>
        }
    }
    render() {
        const { categories } = this.props;
        return (
            <Card>
                <CardBody>
                    {this.renderCategories(categories)}
                </CardBody>
            </Card>
        )
    }
}

const mapStateToProps = ({ categories }) => {
    return { categories: categories.categories };
};


export default connect(mapStateToProps, { fetchAllCategories, deleteCategory })(CategoriesList);