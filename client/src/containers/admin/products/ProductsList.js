import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Card, CardBody, Table } from 'reactstrap';
import { fetchAllProducts, deleteProduct } from '../../../actions/admin/products/actions';

import ProductRow from '../../../components/admin/products/ProductRow';

class ProductsList extends Component {
    constructor(props) {
        super(props);

        this.deleteProductHandler = this.deleteProductHandler.bind(this);
        this.renderProducts = this.renderProducts.bind(this);
    }
    componentDidMount() {
        this.props.fetchAllProducts();
    }
    deleteProductHandler(id) {
        this.props.deleteProduct(id);
    }
    renderProducts(products) {
        if (products.length > 0) {
            return (
                <Table responsive>
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Descripción</th>
                            <th>Categoría</th>
                            <th>Precio</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        {products.map((product, key) => {
                            return (
                                <ProductRow product={product} key={key} onDelete={this.deleteProductHandler}/>
                            );
                        })}
                    </tbody>
                </Table>
            );
        } else {
            return <p>No existen productos.</p>
        }
    }
    render() {
        const { products } = this.props;
        return (
            <Card>
                <CardBody>
                    {this.renderProducts(products)}
                </CardBody>
            </Card>
        )
    }
}

const mapStateToProps = ({ products }) => {
    return { products: products.products };
};


export default connect(mapStateToProps, { fetchAllProducts, deleteProduct })(ProductsList);