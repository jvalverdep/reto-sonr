import React, { Component } from 'react';
import { Row, Col } from 'reactstrap';

import ProductsList from './ProductsList';
import NewProduct from './NewProduct';

export default class extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false
        };
    
        this.toggle = this.toggle.bind(this);
    }
    
    toggle() {
        this.setState(prevState => ({
            showModal: !prevState.showModal
        }));
    }
    
    render() {
        return (
            <div className="animated fadeIn">
                <NewProduct show={this.state.showModal} handleToggle={this.toggle}/>
                <Row style={{height: "1.2rem"}}></Row>
                <Row>
                    <Col size={12}>
                        <button type="button" onClick={this.toggle} className="btn btn-primary">Agregar</button>
                    </Col>
                </Row>
                <br/>
                <Row>
                    <Col size={12}>
                        <ProductsList />
                    </Col>
                </Row>
            </div>
        )
    }
}