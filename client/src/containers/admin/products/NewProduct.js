import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm, Form } from 'redux-form';
import { Modal, ModalHeader, ModalBody, FormGroup } from 'reactstrap';

import { createProduct } from '../../../actions/admin/products/actions';
import { fetchAllCategories } from '../../../actions/admin/categories/actions';

class NewProduct extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            description: '',
            price: '',
            category: ''
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleReset = this.handleReset.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState(prevState => ({
            ...prevState,
            [name]: value
        }));
    }

    handleSubmit(e) {
        e.preventDefault();
        if (this.state.name.trim() && this.state.price.trim() && this.state.category.trim()) {
            this.props.createProduct(this.state);
            this.handleReset();
            this.props.handleToggle();
        }
    }

    handleReset() {
        this.setState({
            name: '',
            description: '',
            price: ''
        });
    }
    
    componentDidMount() {
        this.props.fetchAllCategories().then(() => {
            if (this.props.categories.length > 0) {
                this.setState({ category: this.props.categories[0].id.toString() })
            } else {
                this.setState({ category: '-1' });
            }
        });
    }

    renderCategories(categories) {
        if (categories.length > 0) {
            return (
                <select className="form-control" id="category" name="category" value={this.state.category} onChange={this.handleInputChange}>
                    {categories.map((category, key) => {
                        return (
                            <option value={category.id} key={key}>{category.name}</option>
                        )
                    })}
                </select>
            )
        } else {
            return <p>No existen categorías</p>
        }
    }

    render() {
        return (
            <div>
                <Modal isOpen={this.props.show} toggle={this.props.handleToggle} className={this.props.className}>
                    <ModalHeader toggle={this.props.handleToggle}>Nuevo Producto</ModalHeader>
                    <ModalBody>
                        <Form onSubmit={this.handleSubmit}>
                            <FormGroup>
                                <label htmlFor="name">Nombre</label>
                                <input
                                id="name"
                                name="name"
                                type="text"
                                onChange={this.handleInputChange}
                                value={this.state.name}
                                className="form-control"
                                />
                            </FormGroup>
                            <FormGroup>
                                <label htmlFor="description">Descripción</label>
                                <input
                                id="description"
                                name="description"
                                type="text"
                                onChange={this.handleInputChange}
                                value={this.state.description}
                                className="form-control"
                                />
                            </FormGroup>
                            <FormGroup>
                                <label htmlFor="description">Precio</label>
                                <input
                                id="price"
                                name="price"
                                type="text"
                                onChange={this.handleInputChange}
                                value={this.state.price}
                                className="form-control"
                                />
                            </FormGroup>
                            <FormGroup>
                                <label htmlFor="category">Categoría</label>
                                {this.renderCategories(this.props.categories)}
                            </FormGroup>
                            <button type="submit" className="btn btn-primary">Crear</button>{' '}
                            <button type="button" onClick={this.props.handleToggle} className="btn btn-secondary">Cancelar</button>
                        </Form>
                    </ModalBody>
            </Modal>
          </div>
        )
    }
}

const mapStateToProps = ({ categories }) => {
    return { categories: categories.categories };
};

export default reduxForm({ 
    form: 'productForm' 
})(
    connect(mapStateToProps, { createProduct, fetchAllCategories })(NewProduct)
);