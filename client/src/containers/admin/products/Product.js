import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm, Form } from 'redux-form';
import { NavLink } from 'react-router-dom';
import { Modal, ModalHeader, ModalBody, FormGroup, Container } from 'reactstrap';
import { fetchProduct, updateProduct } from '../../../actions/admin/products/actions';

import { fetchAllCategories } from '../../../actions/admin/categories/actions';

class Product extends Component {
    constructor(props) {
        super(props);
        this.state = {
            product: {
                id: '',
                name: '',
                description: '',
                price: '',
                category_id: ''
            }
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleReset = this.handleReset.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState(prevState => ({
            product: {
                ...prevState.product,
                [name]: value
            }
        }));
    }

    handleSubmit(e) {
        e.preventDefault();
        const { name, description, price, category_id } = this.state.product;
        if (name.trim() && price.trim() && category_id>0) {
            this.props.updateProduct(this.state.product);
            this.handleReset();
            this.props.history.push('/admin/products');
        }
    }

    handleReset() {
        this.setState({
            product: {
                id: '',
                name: '',
                description: '',
                price: '',
                category_id: ''
            }
        });
    }

    componentDidMount() {
        this.props.fetchAllCategories();
        this.props.fetchProduct(this.props.match.params.id).then(() => 
            this.setState({
                product: this.props.product
            })
        );
    }

    renderCategories(categories) {
        if (categories.length > 0) {
            return (
                <select className="form-control" id="category_id" name="category_id" value={this.state.product.category_id} onChange={this.handleInputChange}>
                    {categories.map((category, key) => {
                        return (
                            <option value={category.id} key={key}>{category.name}</option>
                        )
                    })}
                </select>
            )
        } else {
            return <p>No existen categorías</p>
        }
    }

    render() {
        const { product, categories } = this.props;
        if (!product || !categories) {
            return <div>Este producto no existe</div>
        } else {
            return (
                <Container>
                    <Form onSubmit={this.handleSubmit}>
                        <FormGroup>
                            <label htmlFor="name">Nombre</label>
                            <input
                            id="name"
                            name="name"
                            type="text"
                            onChange={this.handleInputChange}
                            value={this.state.product.name}
                            className="form-control"
                            />
                        </FormGroup>
                        <FormGroup>
                            <label htmlFor="description">Descripción</label>
                            <input
                            id="description"
                            name="description"
                            type="text"
                            onChange={this.handleInputChange}
                            value={this.state.product.description}
                            className="form-control"
                            />
                        </FormGroup>
                        <FormGroup>
                            <label htmlFor="description">Precio</label>
                            <input
                            id="price"
                            name="price"
                            type="text"
                            onChange={this.handleInputChange}
                            value={this.state.product.price}
                            className="form-control"
                            />
                        </FormGroup>
                        <FormGroup>
                            <label htmlFor="category">Categoría</label>
                            {this.renderCategories(categories)}
                        </FormGroup>
                        <button type="submit" className="btn btn-primary">Actualizar</button>{' '}
                        {/* <button type="button" className="btn btn-secondary">Cancelar</button> */}
                        <NavLink style={{color: "white"}} to="/admin/products" className="button btn btn-secondary">
                        Cancelar
                        </NavLink>
                    </Form>
                </Container>
            )
        }
    }
}

const mapStateToProps = ({ products, categories }) => {
    return { 
        product: products.product,
        categories: categories.categories
    };
};

export default reduxForm({ 
    form: 'editProductForm' 
})(
    connect(mapStateToProps, { fetchProduct, updateProduct, fetchAllCategories })(Product)
);