import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm, Form } from 'redux-form';
import { Modal, ModalHeader, ModalBody, FormGroup } from 'reactstrap';

import { createUser } from '../../../actions/admin/users/actions';

class NewUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: ''
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleReset = this.handleReset.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState(prevState => ({
            ...prevState,
            [name]: value
        }));
    }

    handleSubmit(e) {
        e.preventDefault();
        if (this.state.username.trim() && this.state.password.trim()) {
            this.props.createUser(this.state);
            this.handleReset();
            this.props.handleToggle();
        }
    }

    handleReset() {
        this.setState({
            username: '',
            password: ''
        });
    }
    
    render() {
        return (
            <div>
                <Modal isOpen={this.props.show} toggle={this.props.handleToggle} className={this.props.className}>
                    <ModalHeader toggle={this.props.handleToggle}>Nuevo Usuario</ModalHeader>
                    <ModalBody>
                        <Form onSubmit={this.handleSubmit}>
                            <FormGroup>
                                <label htmlFor="username">Usuario</label>
                                <input
                                id="username"
                                name="username"
                                type="text"
                                onChange={this.handleInputChange}
                                value={this.state.username}
                                className="form-control"
                                />
                            </FormGroup>
                            <FormGroup>
                                <label htmlFor="password">Contraseña</label>
                                <input
                                id="password"
                                name="password"
                                type="password"
                                onChange={this.handleInputChange}
                                value={this.state.password}
                                className="form-control"
                                />
                            </FormGroup>
                            <button type="submit" className="btn btn-primary">Crear</button>{' '}
                            <button type="button" onClick={this.props.handleToggle} className="btn btn-secondary">Cancelar</button>
                        </Form>
                    </ModalBody>
            </Modal>
          </div>
        )
    }
}

export default reduxForm({ 
    form: 'userForm' 
})(
    connect(null, { createUser })(NewUser)
);