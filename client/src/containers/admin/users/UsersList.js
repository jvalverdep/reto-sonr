import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Card, CardBody, Table } from 'reactstrap';
import { fetchAllUsers, deleteUser } from '../../../actions/admin/users/actions';

import User from '../../../components/admin/users/User';

class UsersList extends Component {
    constructor(props) {
        super(props);

        this.deleteUserHandler = this.deleteUserHandler.bind(this);
        this.renderUsers = this.renderUsers.bind(this);
    }
    componentDidMount() {
        this.props.fetchAllUsers();
    }
    deleteUserHandler(id) {
        this.props.deleteUser(id);
    }
    renderUsers(users) {
        if (users.length > 0) {
            return (
                <Table responsive>
                    <thead>
                        <tr>
                            <th>Nombre de Usuario</th>
                            <th>Contraseña</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        {users.map((user, key) => {
                            return (
                                <User user={user} key={key} onDelete={this.deleteUserHandler}/>
                            );
                        })}
                    </tbody>
                </Table>
            );
        } else {
            return <p>No existen usuarios.</p>
        }
    }
    render() {
        const { users } = this.props;
        return (
            <Card>
                <CardBody>
                    {this.renderUsers(users)}
                </CardBody>
            </Card>
        )
    }
}

const mapStateToProps = ({ users }) => {
    return { users: users.users };
};


export default connect(mapStateToProps, { fetchAllUsers, deleteUser })(UsersList);