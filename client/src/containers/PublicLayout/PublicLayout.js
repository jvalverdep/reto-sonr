import React from 'react';
import PublicHeader from './PublicHeader';
import ProductsContainer from '../public/ProductsContainer';
import Product from '../public/Product';

export default class PublicLayout extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showProduct: false,
            focusProduct: ''
        }
        this.toggle = this.toggle.bind(this);
        this.showProduct = this.showProduct.bind(this);
    }

    toggle(e, id) {
        e.preventDefault();
        this.setState({
            focusProduct: id,
            showProduct: true
        });
    }

    showProduct() {
        this.setState({
            showProduct: false
        })
    }

    render() {
        return (
            <div>
                <PublicHeader clickHandler={this.showProduct}/>
                {this.state.showProduct !== true ? <ProductsContainer renderProduct={this.toggle}/> : <Product show={this.state.showProduct} id={this.state.focusProduct}/>}
                {/* <ProductsContainer /> */}
            </div>
        );
    }
}