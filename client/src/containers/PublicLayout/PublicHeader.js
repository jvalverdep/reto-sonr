import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import {
    Collapse,
    Navbar,
    NavbarToggler
} from 'reactstrap';

import { AppNavbarBrand } from '@coreui/react';
import logo from '../../assets/img/brand/logo.svg'
import sygnet from '../../assets/img/brand/sygnet.svg'

import CategoriesNav from '../public/CategoriesNav';

import { fetchAllProducts } from '../../actions/index';

class PublicHeader extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false
        };
        this.handleClick = this.handleClick.bind(this);
    }
    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }
    handleClick(e) {
        e.preventDefault();
        this.props.fetchAllProducts();
    }
    render() {
        return (
            <div>
                <Navbar color="white" light expand="md">
                    {/* <NavLink to="/" className="navbar-brand" onClick={this.handleClick}>Home</NavLink> */}
                    <AppNavbarBrand className="active" onClick={this.handleClick} style={{cursor: "pointer"}}
                        minimized={{ src: sygnet, width: 30, height: 30, alt: 'CoreUI Logo' }}
                    />
                    <NavLink to="/admin" className="navbar-brand ml-auto">Administrador</NavLink>
                </Navbar>
                <Navbar color="light" light expand="md">
                    <NavbarToggler onClick={this.toggle} />
                    <Collapse isOpen={this.state.isOpen} navbar>
                        <CategoriesNav clickCategory={this.props.clickHandler} />
                    </Collapse>
                </Navbar>
            </div>            
        )
    }
}

export default connect(null, { fetchAllProducts })(PublicHeader);