import React, { Component } from 'react';
import { connect } from 'react-redux';

import { fetchProduct } from '../../actions/index';

import {
    Row, Col, Container,
    Card, Button, CardImg, CardTitle, CardText, CardDeck,
    CardSubtitle, CardBody, CardFooter
} from 'reactstrap';

class Product extends Component {
    constructor(props) {
        super(props);
        this.state = {
            product: null
        }
    }

    componentDidMount() {
        this.props.fetchProduct(this.props.id).then(() => { console.log(this.props.product); this.setState({ product: this.props.product })});
    }
    render() {
        const { product } = this.state;
        if (product !== null) {
            return (
                <div style={{marginTop: "1.2rem"}}>
                    <Row style={{margin: "0"}}>
                        <Col sm="6">
                            <Card>
                                <CardImg top width="100%" src="https://placeholdit.imgix.net/~text?txtsize=33&txt=256%C3%97180&w=256&h=180" alt="Card image cap" />
                            </Card>
                        </Col>
                        <Col sm="6">
                            <Card>
                                <CardBody>
                                    <div style={{display: "flex", flexFlow:"row nowrap", justifyContent: "flex-start"}}>
                                        <h4>{product.name}</h4>
                                        <h5 className="ml-auto">{product.category.name}</h5>
                                    </div>
                                    <CardText style={{marginBottom: "0.75rem"}}>S/. {product.price}</CardText>
                                    <CardText>{product.description}</CardText>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </div>
            )
        } else {
            return <div>No hay</div>
        }        
    }
}

const mapStateToProps = ({ products }) => {
    return { product: products.product };
};

export default connect(mapStateToProps, { fetchProduct })(Product);