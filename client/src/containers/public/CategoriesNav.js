import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Nav, NavItem } from 'reactstrap';
import { NavLink } from 'react-router-dom';

import { fetchAllCategories, filterProductsByCategoryId } from '../../actions/index';

class CategoriesNav extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
        this.handleClick = this.handleClick.bind(this);
    }
    handleClick(e, id) {
        e.preventDefault();
        this.props.filterProductsByCategoryId(id)
        this.props.clickCategory();
    }
    componentDidMount() {
        this.props.fetchAllCategories();
    }

    renderCategories(categories) {
        return categories.map((category, key) => {
            return (
                <NavItem key={key}>
                    <NavLink to="#"  className="nav-link"
                    onClick={e => this.handleClick(e, category.id)}>
                        {category.name}
                    </NavLink>
                </NavItem>
            )
        });
    }
    render() {
        const { categories } = this.props;
        return (
            <Nav className="ml-auto" navbar>
                {this.renderCategories(categories)}
            </Nav>
        )
    }
}

const mapStateToProps = ({ categories }) => {
    return { categories: categories.categories };
};

export default connect(mapStateToProps, { fetchAllCategories, filterProductsByCategoryId })(CategoriesNav);