import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';

import {
    Row, Col, Container,
    Card, Button, CardImg, CardTitle, CardText, CardDeck,
    CardSubtitle, CardBody, CardFooter
} from 'reactstrap';

import { fetchAllProducts } from '../../actions/index';

class ProductsContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(e) {
        e.preventDefault();
    }

    componentDidMount() {
        this.props.fetchAllProducts();
    }

    renderProducts(products) {
        if (products.length === 0) {
            return <div>No existen products</div>
        } else {
            return products.map((product, key) => {
                return (
                    <Col sm="3" key={key}>
                        <Card>
                            <CardImg top width="100%" src="https://placeholdit.imgix.net/~text?txtsize=33&txt=256%C3%97180&w=256&h=180" alt="Card image cap" />
                            <CardBody>
                                <CardTitle>{product.name}</CardTitle>
                                <CardSubtitle>{product.category.name}</CardSubtitle>
                                <CardText>{product.description}</CardText>
                                <p style={{textAlign: "right"}}>{product.price}$</p>
                            </CardBody>
                            <CardFooter>
                                <Button onClick={e => this.props.renderProduct(e, product.id)}>Ver</Button>
                            </CardFooter>
                        </Card>
                    </Col>
                    
                )
            });
        }
    }
    render() {
        const { products } = this.props;
        return (
            <Container style={{marginTop:"1.2rem"}}>
                <Row>
                    {this.renderProducts(products)}
                </Row>
            </Container>
        )
    }
}

const mapStateToProps = ({ products }) => {
    return { products: products.products };
};

export default connect(mapStateToProps, { fetchAllProducts })(ProductsContainer);