import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm, Form } from 'redux-form';
import { Container, Row, FormGroup } from 'reactstrap';

import { signInUser } from '../../actions/auth';

class SignIn extends Component {
    constructor(props) {
        super(props)
        
        this.state = {
            username: '',
            password: ''
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState(prevState => ({
            ...prevState,
            [name]: value
        }));
    }

    handleSubmit(e) {
        e.preventDefault();
        if (this.state.username.trim() && this.state.password.trim()) {
            this.props.signInUser(this.state);
        }
    }

    render() {
        return (
            <Container>
                <Row>
                    <Form onSubmit={this.handleSubmit}>
                        <FormGroup>
                            <label htmlFor="username">Usuario</label>
                            <input
                            id="username"
                            name="username"
                            type="text"
                            onChange={this.handleInputChange}
                            value={this.state.username}
                            className="form-control"
                            />
                            {/* <Label for="username">Username</Label>
                            <Input type="username" name="username" id="username" placeholder="Username"/> */}
                        </FormGroup>
                        <FormGroup>
                            <label htmlFor="password">Contraseña</label>
                            <input
                            id="password"
                            name="password"
                            type="password"
                            onChange={this.handleInputChange}
                            value={this.state.password}
                            className="form-control"
                            />
                        </FormGroup>
                        <button type="submit">SignIn</button>
                    </Form>
                </Row>
            </Container>
        )
    }
}

export default reduxForm({ 
    form: 'signIn' 
})(
    connect(null, { signInUser })(SignIn)
);