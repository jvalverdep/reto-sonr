import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

export default function(ComposedComponent) {
  class Authentication extends Component {
    static contextTypes = {
        router: PropTypes.object.isRequired
    }

    componentWillMount() {
        if (!this.props.authenticated) {
            this.context.router.history.push('/auth/signin');
        }
    }

    componentWillUpdate(nextProps) {
        if (!nextProps.authenticated) {
            this.context.router.history.push('/auth/signin');
        }
    }
    
    render() {
        return <ComposedComponent {...this.props} />
    }
  }

  function mapStateToProps({ auth }) {
    return { authenticated: auth.authenticated };
  }

  return connect(mapStateToProps)(Authentication);
}