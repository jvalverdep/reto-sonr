import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { signOutUser } from '../../actions/auth';

class Signout extends Component {
    static contextTypes = {
        router: PropTypes.object.isRequired
    }
    esperarAlgo() {
        console.log('Ya deberias irte');
        this.context.router.history.push('/');
    }
    componentDidMount() {
        setTimeout(this.esperarAlgo.bind(this), 3000);
    }
    componentWillMount() {
        this.props.signOutUser();
    }
    render() {
        return (
            <div>
                Sorry to see you go...
            </div>
        );
    }
}

export default connect(null, { signOutUser })(Signout);