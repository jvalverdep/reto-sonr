import React from 'react';

import './User.css';

export default ({ user: { id, username, password }, onDelete}) => {
    return (
        <tr>
            <td>{ username }</td>
            <td>{ password }</td>
            <td className="user-options">
                {/* <button className="option-1">
                    <i className="fa fa-edit" aria-hidden="true"></i>
                </button> */}
                <button onClick={() => onDelete(id)}>
                    <i className="fa fa-trash" aria-hidden="true"></i>
                </button>
            </td>
        </tr>
    )
}