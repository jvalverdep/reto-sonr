import React from 'react';

import { NavLink } from 'react-router-dom';
import './ProductRow.css';

export default ({ product: { id, name, description, price, category }, onDelete}) => {
    const pathToProduct = `/admin/products/${id}`;
    return (
        <tr>
            <td>{ name }</td>
            <td>{ description }</td>
            <td>{ category.name }</td>
            <td>{ price }</td>
            <td className="product-options">
                <NavLink style={{color: "black"}} to={pathToProduct} className="button option-1">
                    <i className="fa fa-edit" aria-hidden="true"></i>
                </NavLink>
                <button onClick={() => onDelete(id)}>
                    <i className="fa fa-trash" aria-hidden="true"></i>
                </button>
            </td>
        </tr>
    )
}