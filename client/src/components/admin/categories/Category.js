import React from 'react';

import { NavLink } from 'react-router-dom';
import './Category.css';

export default ({ category: { name, description, id }, onDelete}) => {
    const pathToCategory = `/admin/categories/${id}`;
    return (
        <tr>
            <td>{ name }</td>
            <td>{ description }</td>
            <td className="category-options">
                <NavLink style={{color: "black"}} to={pathToCategory} className="button option-1">
                    <i className="fa fa-edit" aria-hidden="true"></i>
                </NavLink>
                <button onClick={() => onDelete(id)}>
                    <i className="fa fa-trash" aria-hidden="true"></i>
                </button>
            </td>
        </tr>
    )
}