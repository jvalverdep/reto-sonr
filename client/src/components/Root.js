import React, { lazy, Suspense } from 'react';
import PropTypes from 'prop-types'
import { Provider } from 'react-redux';
import { Router, Route, Switch } from 'react-router-dom';
import ReduxToastr from 'react-redux-toastr';
import createBrowserHistory from 'history/createBrowserHistory';
export const browserHistory = createBrowserHistory();

import Loadable from 'react-loadable';
import RequireAuth from '../containers/auth/RequireAuth';

const loading = () => <div className="animated fadeIn pt-3 text-center">Loading...</div>;

import SignOut from '../containers/auth/SignOut';

// Containers
const AdminLayout = Loadable({
    loader: () => import('../containers/AdminLayout/AdminLayout'),
    loading
});
const SignIn = Loadable({
    loader: () => import('../containers/auth/SignIn'),
    loading
});
const PublicLayout = Loadable({
    loader: () => import('../containers/PublicLayout/PublicLayout'),
    loading
});

const Root = ({ store }) => (
    <Provider store={store}>
        <Router history={browserHistory}>
            <Switch>
                <Route exact path="/auth/signin" name="SignIn" component={SignIn} />
                <Route exact path="/auth/signout" name="SignOut" component={SignOut} />
                
                <Route path="/admin" name="Admin" component={RequireAuth(AdminLayout)} />
                <Route path="/" name="Home" component={PublicLayout} />
            </Switch>
        </Router>
        <ReduxToastr
            timeOut={4000}
            newestOnTop={false}
            preventDuplicates
            position="top-left"
            transitionIn="fadeIn"
            transitionOut="fadeOut"
            progressBar
            closeOnToastrClick />
    </Provider>
  );

Root.propTypes = {
    store: PropTypes.object.isRequired
}

export default Root;